import "./App.css";
import { Route, Routes } from "react-router-dom";
import { DetailPage, SearchPage } from "./components";
import { useState } from "react";

function App() {
  const [repos, setRepos] = useState([]);
  return (
    <Routes>
      <Route
        path="/"
        element={<SearchPage repos={repos} setRepos={setRepos} />}
      />
      <Route path="/detail/:repoName" element={<DetailPage repos={repos} />} />
    </Routes>
  );
}

export default App;
