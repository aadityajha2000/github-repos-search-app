import { SearchIcon } from "@heroicons/react/outline";
import React, { useCallback, useEffect, useState } from "react";
import { useRef } from "react";

const SearchBar = ({ value, setValue, handleSearch }) => {
  const searchDivRef = useRef(null);
  const searchRef = useRef(null);
  const [isFocus, setIsFocus] = useState(true);

  const handleClickFocus = () => {
    if (isFocus) {
      searchRef.current.focus();
      setIsFocus(!isFocus);
    } else {
      searchRef.current.blur();
      setIsFocus(!isFocus);
    }
  };

  const handleFocus = useCallback((event) => {
    const activeElement = document.activeElement;
    if (event.ctrlKey && event.key === "k") {
      event.preventDefault();

      if (searchRef && searchRef.current) {
        setIsFocus((prevState) => {
          console.log(prevState);
          if (!prevState) {
            searchRef.current.blur();
            return true;
          }
          searchRef.current.focus();
          return false;
        });
      }
    }

    if (event.key === "Escape") {
      if (searchRef === activeElement) {
        searchRef.current.blur();
      }
    }
  });
  useEffect(() => {
    document.addEventListener("keydown", handleFocus);

    return () => {
      document.removeEventListener("keydown", handleFocus);
    };
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    handleSearch();
  };

  return (
    <button
      ref={searchDivRef}
      className={`flex flex-row ml-auto rounded-md items-center py-1.5 px-4 w-full  gap-x-2 focus:outline-none focus:shadow-lg focus-within:shadow-lg border border-gray-300  focus-within:ring-2 focus-within:ring-cyan-500 `}
      onClick={handleClickFocus}
    >
      <span onClick={handleClickFocus}>
        <SearchIcon className="h-4 w-4 text-gray-500" />
      </span>
      <form onSubmit={handleSubmit}>
        <input
          type="search"
          placeholder="Search"
          className=" focus:outline-none w-4/6"
          onBlur={() => setIsFocus(!isFocus)}
          value={value}
          ref={searchRef}
          onChange={(e) => {
            setValue(e.target.value);
          }}
        />
      </form>
      <span
        className={`py-0.5 px-1 border border-gray-400 rounded-md text-sm text-gray-400 ml-auto mr-2`}
        onClick={handleClickFocus}
      >
        Ctrl + K
      </span>
    </button>
  );
};

export default SearchBar;
