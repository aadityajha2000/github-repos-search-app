import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactMarkdown from "react-markdown";
import { useParams, Link } from "react-router-dom";
import { getReadme } from "../../api/search";
import { baseUrl, githubUrl } from "../../utils/constants";

const DetailPage = ({ repos }) => {
  const { repoName } = useParams();
  const [repo, setRepo] = useState(null);
  const [readMe, setReadMe] = useState("");
  useEffect(() => {
    console.log(repoName);
    const repository = repos.filter((r) => r.name === repoName)[0];
    console.log(repository);
    setRepo(repository);
  }, [repoName]);

  useEffect(() => {
    if (repo) {
      getReadme(repo.owner.login, repo.name, setReadMe);
    }
  }, [repo]);
  useEffect(() => {
    console.log(readMe);
  }, [readMe]);

  if (!repo)
    return (
      <div className="h-screen w-screen flex items-center justify-center text-2xl font-bold ">
        Repo Not found. Please go to Home page.
      </div>
    );
  return (
    <>
      <div className="flex flex-col mt-10 w-full md:w-3/6 items-center justify-center px-6">
        <div className="flex flex-row items-center w-full gap-x-2 mb-5">
          <Link to="/">
            <span className="text-blue-500 hover:underline">Home</span>
          </Link>
          <span className="text-gray-500">&#47;</span>
          <span>{repo.name}</span>
        </div>
        <div className="flex flex-row gap-x-2 items-center justify-start text-2xl font-bold w-full">
          <a target="_blank" href={githubUrl + repo.owner.login}>
            <span className="text-blue-500 hover:underline">
              {repo.owner.login}
            </span>
          </a>
          <span className="text-gray-500">&#47;</span>
          <a href={githubUrl + repo.full_name}>
            <span className="text-blue-500 hover:underline">{repo.name}</span>
          </a>
        </div>
        <div className="flex flex-row gap-x-2 items-center justify-start font-medium text-base text-gray-500 w-full">
          <span className="text-base text-gray-500">
            Open Issues: {repo.open_issues_count}
          </span>
          <span>Default Branch: {repo.default_branch}</span>
        </div>
      </div>
      <div className="flex flex-col w-full md:w-5/6 justify-center border border-gray-300 rounded-md px-5 py-3 mx-5">
        <div className="text-blue-500 hover:underline border-b border-gray-400 py-2">
          README.md
        </div>
        {readMe ? <ReactMarkdown>{readMe}</ReactMarkdown> : null}
      </div>
    </>
  );
};

export default DetailPage;
