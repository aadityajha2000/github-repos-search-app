export const sortItems = [
  {
    id: 1,
    name: "Best Match",
    value: "best-match",
  },
  {
    id: 2,
    name: " Stars",
    value: "stars",
  },
  {
    id: 4,
    name: "Forks",
    value: "forks",
  },
  {
    id: 5,
    name: "Recently Updated",
    value: "updated",
  },
];
