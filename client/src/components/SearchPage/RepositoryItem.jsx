import React from "react";
import { RiGitRepositoryLine } from "react-icons/ri";
import { AiOutlineStar, AiFillEye } from "react-icons/ai";
import { BiGitRepoForked } from "react-icons/bi";
import { formatter } from "../../utils/helpers";
import { Link } from "react-router-dom";
import moment from "moment";
const RepositoryItem = ({ repo }) => {
  return (
    <div className="flex flex-col w-full md:w-3/5 border-b border-gray-300">
      <div className="flex flex-row gap-x-2 px-4 py-6">
        <span>
          <RiGitRepositoryLine fontSize={24} className="text-neutral-600" />
        </span>
        <div className="flex flex-col gap-y-2">
          <Link to={`/detail/${repo.name}`}>
            <span className="hover:underline text-blue-500 text-lg font-bold cursor-pointer">
              {repo.full_name}
            </span>
          </Link>
          <div className="flex flex-row items-center gap-x-2">
            <div class="relative w-4 h-4 overflow-hidden bg-gray-100 rounded-full dark:bg-gray-600">
              <img src={repo.owner.avatar_url} alt="Owner Avatar" />
            </div>
            <a target="_blank" href={"https://github.com/" + repo.owner.login}>
              <span className="text-base text-blue-500 hover:underline">
                {repo.owner.login}
              </span>
            </a>
          </div>
          <span className="text-gray-600 text-sm">{repo.description}</span>
          {repo.topics && repo.topics.length ? (
            <div className="flex flex-wrap text-sm gap-x-4 gap-y-2">
              {repo.topics.map((topic, index) => (
                <a
                  key={index}
                  target="_blank"
                  href={"https://github.com/topics/" + topic}
                >
                  <span className="inline-block flex justify-center rounded-full border-2 border-sky-500 text-sky-500 hover:bg-blue-100 transition px-2 py-0.5 cursor-pointer ">
                    {topic}
                  </span>
                </a>
              ))}
            </div>
          ) : null}
          <div className="flex flex-row gap-x-4 text-sm text-gray-400">
            <span className="flex items-center">
              <AiOutlineStar className="inline-block " fontSize={16} />{" "}
              {formatter.format(repo.stargazers_count)}
            </span>
            <span className="flex items-center">
              <BiGitRepoForked className="inline-block " fontSize={16} />{" "}
              {formatter.format(repo.forks_count)}
            </span>
            <span className="flex items-center">
              <AiFillEye className="inline-block " fontSize={16} />{" "}
              {formatter.format(repo.watchers_count)}
            </span>
            <span className="flex items-center">
              Updated on {moment(repo.updated_at).format("ll")}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RepositoryItem;
