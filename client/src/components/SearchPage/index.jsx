import React, { useCallback, useEffect, useRef, useState } from "react";
import { searchRepos } from "../../api/search";
import { getTotalPage } from "../../utils/helpers";
import SearchBar from "../SearchBar/SearchBar";
import SortMenu from "../SortMenu/SortMenu";
import FilterBar from "./FilterBar";
import RepositoryItem from "./RepositoryItem";
import { sortItems } from "./sortItems";
import moment from "moment";

const SearchPage = ({ repos, setRepos }) => {
  const [search, setSearch] = useState("");

  const [page, setPage] = useState(1);
  const [page_size, setPage_size] = useState(5);
  const [sort, setSort] = useState(sortItems[0]);
  // const [order, setOrder] = useState(null);
  const [totalData, setTotalData] = useState();
  const [loading, setloading] = useState(false);

  useEffect(() => {
    console.log(repos);
  }, [repos]);

  useEffect(() => {
    handleSearch();
  }, [sort]);

  const handleSearch = () => {
    searchRepos(
      search,
      page,
      page_size,
      sort.value,
      setRepos,
      setTotalData,
      setloading
    );
  };

  const observer = useRef();
  const lastVendorElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (
          entries[0].isIntersecting &&
          getTotalPage(totalData, page_size) >= page
        ) {
          console.log("Visible");

          searchRepos(
            search,
            page + 1,
            page_size,
            sort.value,
            setRepos,
            setTotalData,
            setloading,
            true
          );
          setPage((prevPage) => prevPage + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading]
  );
  return (
    <div className="flex flex-col mt-10 w-full items-center justify-center">
      <div className="flex flex-row items-center w-full justify-center gap-x-4 px-4">
        <div className="md:w-3/6 w-full">
          <SearchBar
            value={search}
            setValue={setSearch}
            handleSearch={handleSearch}
          />
        </div>
        <button
          type="button"
          onClick={handleSearch}
          className="px-4 py-2 rounded-md bg-cyan-500 text-white shadow-md hover:bg-cyan-600 transition"
        >
          Search
        </button>
      </div>
      {repos.length ? (
        <>
          <FilterBar
            sort={sort}
            setSort={setSort}
            totalData={totalData}
            length={repos.length}
          />
        </>
      ) : null}
      {repos.map((repo) => (
        <RepositoryItem key={repo.id} repo={repo} />
      ))}
      {repos.length ? <div ref={lastVendorElementRef} /> : null}
    </div>
  );
};

export default SearchPage;
