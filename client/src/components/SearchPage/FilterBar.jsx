import React from "react";
import SortMenu from "../SortMenu/SortMenu";

const FilterBar = ({ sort, setSort, totalData, length }) => {
  return (
    <div className="flex flex-row items-center mt-5 p-4 md:w-3/5 justify-between w-full border-b border-gray-300">
      <div className="flex flex-row">
        <h1 className="text-2xl font-semibold text-gray-600">
          Total <span>{totalData}</span> results
        </h1>
        <div className="ml-2 text-sm text-gray-400 self-end">
          Displaying {length} repos
        </div>
      </div>
      <div className="ml-auto w-2/6">
        <SortMenu selected={sort} setSelected={setSort} />
      </div>
    </div>
  );
};

export default FilterBar;
