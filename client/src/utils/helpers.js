export const formatter = Intl.NumberFormat("en", { notation: "compact" });

export const getTotalPage = (totalData, size) => {
  return Math.ceil(totalData / size);
};
