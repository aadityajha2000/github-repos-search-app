import axios from "axios";
import { baseUrl } from "../utils/constants";

export const searchRepos = (
  query,
  page,
  page_size,
  sort,
  setData,
  setTotalData,
  setLoading,
  isPagination
) => {
  setLoading(true);
  const config = {
    params: {
      query,
      page,
      page_size,
      sort,
    },
  };
  return axios.get(baseUrl + "api/search", config).then(({ data }) => {
    if (data.success) {
      if (isPagination) {
        setData((prevState) => [...prevState, ...data.data.items]);
      } else {
        setData(data.data.items);
        setTotalData(data.data.total_count);
      }
      setLoading(false);
    }
  });
};

export const getReadme = (owner, repo_name, setMarkDown) => {
  return axios
    .get(baseUrl + `api/search/readme`, {
      params: { owner, repo_name },
    })
    .then(({ data }) => {
      if (data.success) {
        setMarkDown(data.data);
      }
    });
};
