const responseHelper = require("../../helpers/response.helper");
const httpStatus = require("http-status");
const axios = require("axios");
const { HEADER_ACCEPT, baseUrl, RAW_HEADER_ACCEPT } = require("../../config");

const searchReposController = {};

searchReposController.search = async (req, res, next) => {
  try {
    const query = req.query.query;
    const page = req.query.page || 1;
    const sort = req.query.sort;
    const page_size = req.query.page_size || 5;
    const order = req.query.order;
    const config = {
      headers: {
        Accept: HEADER_ACCEPT,
      },
      params: {
        q: query,
        per_page: page_size,
        page,
        order,
        sort,
      },
    };
    const response = await axios.get(baseUrl + "search/repositories", config);
    console.log(response.status);
    const { data } = response;
    if (response.status === 200 || response.status === 304) {
      return responseHelper.sendResponse(
        res,
        httpStatus.OK,
        true,
        data,
        null,
        "Search repos success"
      );
    } else {
      return responseHelper.sendResponse(
        res,
        httpStatus.OK,
        false,
        null,
        response.statusText,
        response.statusText
      );
    }
  } catch (err) {
    responseHelper.sendResponse(res, httpStatus.OK, false, null, err, err);
  }
};

searchReposController.getReadMeContent = async (req, res, next) => {
  const owner = req.query.owner;
  const repo_name = req.query.repo_name;
  const config = {
    headers: {
      Accept: RAW_HEADER_ACCEPT,
    },
  };
  const { data } = await axios.get(
    baseUrl + `repos/${owner}/${repo_name}/readme`,
    config
  );
  return responseHelper.sendResponse(
    res,
    httpStatus.OK,
    true,
    data,
    null,
    "README file get success"
  );
};

module.exports = searchReposController;
